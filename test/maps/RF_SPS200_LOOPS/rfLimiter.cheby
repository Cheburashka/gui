memory-map:
  name: rfLimiter
  bus: axi4-lite-32
  size: 4k
  word-endian: little
  schema-version:
    core: 2.0.0
    x-conversions: 1.0.0
    x-driver-edge: 1.0.0
    x-enums: 1.0.0
    x-fesa: 2.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-map-info: 1.0.0
    x-wbgen: 1.0.0
  x-map-info:
    memmap-version: 1.4.0
  x-hdl:
    bus-attribute: Xilinx
    name-suffix: _regs
    bus-granularity: byte
  children:
    - reg:
        name: limitingLevel
        width: 16
        access: rw
        address: next
        type: unsigned
        x-fesa:
          multiplexed: false
          persistence: True
        x-conversions:
          read: val/pow(2.0, 15.0)
          write: val*pow(2.0, 15.0)
        x-driver-edge:
          min-val: 0
          max-val: 32767
    - reg:
        name: controls
        width: 32
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: false
        children:
          - field:
              name: outputEna
              description: If false, output IQ data forced to zero
              range: 0
          - field:
              name: clearStatus
              description: clear latched status/interlocks
              range: 1
              x-hdl:
                type: autoclear
          - field:
              name: clearInterlocks
              description: Clear sum of interlocks
              range: 2
              x-hdl:
                type: autoclear
          - field:
              name: localInterlock
              description: Local/Soft interlock
              range: 3
    - reg:
        name: status
        width: 32
        access: ro
        address: next
        x-fesa:
          multiplexed: false
        x-hdl:
          port: reg
        children:
          - field:
              name: limiting
              range: 0
          - field:
              name: amcInterlock
              description: AMC interlock, True = active
              range: 1
          - field:
              name: rfSwitchOn
              description: RF switch state, True= rf switch closed
              range: 2
          - field:
              name: sumInterlock
              description: Sum of all active interlocks (drive the AMC interlock line)
              range: 3
          - field:
              name: localVeto
              description: Interlock from control register (local/soft)
              range: 4
          - field:
              name: fpInterlock
              description: RTM front panel interlock, True = active
              range: 5
          - field:
              name: demodulatorInterlock
              description: IQ demodualtor status
              range: 6
          - field:
              name: modulatorInterlock
              description: IQ modulator status
              range: 7
          - field:
              name: clockInterlock
              description: "DSP clock error: Wrong source clock, reset active, adc/dac calibration not done"
              range: 8
          - field:
              name: ftwH1FreqInterlock
              description: FTW H1 out of bounds
              range: 9
          - field:
              name: ftwH1OnFreqInterlock
              description: FTW H1 ON out of bounds
              range: 10
          - field:
              name: fgcInterlock
              description: Function Generator interlock
              range: 11
          - field:
              name: rfncoInterlock
              description: RFNCO interlock
              range: 12
          - field:
              name: vspInterlock
              description: Voltage setpoint interlock
              range: 13
    - reg:
        name: latchedStatus
        description: latched register. Cleared by control bits clearStatus or clearVeto
        comment: "LatchedStatus bits can be masked by the InterlockMask register bits\nclear by clearStatus"
        width: 32
        access: ro
        address: next
        x-fesa:
          multiplexed: True
        x-hdl:
          port: reg
        children:
          - field:
              name: limiting
              description: RF limiter on
              range: 0
          - field:
              name: localVeto
              description: Interlock from control register (local/soft)
              range: 4
          - field:
              name: fpInterlock
              description: RTM front panel interlock, True = active
              range: 5
          - field:
              name: demodulatorInterlock
              description: IQ demodualtor status
              range: 6
          - field:
              name: modulatorInterlock
              description: IQ modulator status
              range: 7
          - field:
              name: clockInterlock
              description: "DSP clock error: Wrong source clock, reset active"
              range: 8
          - field:
              name: ftwH1FreqInterlock
              description: FTW H1 out of bounds
              range: 9
          - field:
              name: ftwH1OnFreqInterlock
              description: FTW H1 ON out of bounds
              range: 10
          - field:
              name: fgcInterlock
              description: Function Generator interlock
              range: 11
          - field:
              name: rfnco
              description: RFNCO interlock
              range: 12
          - field:
              name: vspInterlock
              description: Voltage setpoint interlock
              range: 13
    - reg:
        name: interlockMask
        description: Mask for interlocks acting on the RF switch
        width: 32
        access: rw
        address: next
        x-fesa:
          multiplexed: False
          persistence: True
        children:
          - field:
              name: fpInterlock
              description: RTM front panel interlock (external)
              range: 1
          - field:
              name: demodulatorInterlock
              description: IQ demodualtor status
              range: 2
          - field:
              name: modulatorInterlock
              description: IQ modulator status
              range: 3
          - field:
              name: clockInterlock
              description: "DSP clock error: Wrong source clock"
              range: 4
          - field:
              name: ftwH1FreqInterlock
              description: ftw_h1 and ftw_h1_on ON out of bounds
              range: 5
          - field:
              name: fgcInterlock
              description: Function Generator interlock
              range: 6
          - field:
              name: rfncoInterlock
              description: RFNCO interlock
              range: 7
          - field:
              name: vspInterlock
              description: Voltage setpoint interlock
              range: 8
    - block:
        name: timestamp
        children:
          - reg:
              name: limiting
              width: 64
              access: ro
              description: timestamp of the last limiting status
              x-hdl:
                port: reg
              x-fesa:
                multiplexed: true
              children:
                - field:
                    name: tm_tai
                    range: 63-32
                    description: UTC time
                    x-fesa:
                      unit: s
                - field:
                    name: tm_cycle
                    range: 31-0
                    description: wr clock cycle within the last second tick
                    x-fesa:
                      unit: s
                    x-conversions:
                      read: val/62.5e6
          - reg:
              name: interlock
              width: 64
              access: ro
              description: timestamp of the last sumInterlock status
              x-hdl:
                port: reg
              x-fesa:
                multiplexed: true
              children:
                - field:
                    name: tm_tai
                    range: 63-32
                    description: UTC time
                    x-fesa:
                      unit: s
                - field:
                    name: tm_cycle
                    range: 31-0
                    description: wr clock cycle within the last second tick
                    x-fesa:
                      unit: s
                    x-conversions:
                      read: val/62.5e6