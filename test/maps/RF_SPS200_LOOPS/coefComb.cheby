memory-map:
  name: coefComb
  bus: axi4-lite-32
  size: 4k
  x-map-info:
    memmap-version: 2.1.1
  schema-version:
    core: 2.0.0
    x-conversions: 1.0.0
    x-driver-edge: 1.0.0
    x-fesa: 2.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-map-info: 1.0.0
    x-wbgen: 1.0.0
    x-enums: 1.0.0
  x-hdl:
    name-suffix: _regs
    bus-attribute: Xilinx
    bus-granularity: byte
  children:
    - reg:
        name: control
        description: Control Comb IIR filters
        width: 32
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        x-hdl:
          port: field
        children:
          - field:
              name: load
              range: 0
              description: trigger coefficients loading (if static coefficients)
              x-hdl:
                type: autoclear
          - field:
              name: static_en
              range: 1
              description: enable static coefficients for comb filters. Otherwise coefficient from functions
    - reg:
        name: f1b0
        description: Comb IIR filter 1 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f1b1
        description: Comb IIR filter 1 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f1b2
        description: Comb IIR filter 1 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f1a1
        description: Comb IIR filter 1 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f1a2
        description: Comb IIR filter 1 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f2b0
        description: Comb IIR filter 2 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f2b1
        description: Comb IIR filter 2 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f2b2
        description: Comb IIR filter 2 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f2a1
        description: Comb IIR filter 2 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f2a2
        description: Comb IIR filter 2 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f3b0
        description: Comb IIR filter 3 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f3b1
        description: Comb IIR filter 3 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f3b2
        description: Comb IIR filter 3 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f3a1
        description: Comb IIR filter 3 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071
    - reg:
        name: f3a2
        description: Comb IIR filter 3 coefficent
        width: 32
        type: signed
        access: rw
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: data
              range: 17-0
              type: signed
              x-conversions:
                read: val / pow(2.0,16.0)
                write: val * pow(2.0,16.0)
              x-driver-edge:
                min-val: -131072
                max-val: 131071