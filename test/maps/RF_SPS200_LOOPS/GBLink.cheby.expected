memory-map:
  name: gbLink
  description: SPS gigabit serial link for beam synchronous and fixed-clock data
  bus: axi4-lite-32
  size: 4k
  x-gena:
    map-version: 20211102
  x-driver-edge:
    module-type: RF_GBLINK
  x-map-info:
    ident: 0x00
    memmap-version: 1.0.0
  schema-version:
    core: 3.0.0
    x-conversions: 1.0.0
    x-driver-edge: 3.0.0
    x-fesa: 2.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-map-info: 1.0.0
    x-wbgen: 1.0.0
    x-enums: 1.0.0
  x-hdl:
    bus-granularity: byte
    bus-attribute: Xilinx
    name-suffix: _regs
  children:
    - submap:
        name: ipInfo
        address: next
        filename: ipInfo.cheby
        include: true
    - reg:
        name: control
        description: Control register
        width: 32
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: false
        children:
          - field:
              name: reset
              range: 0
              preset: 0x0
              description: Global synchronous reset that puts all common synchronous logic to a known state
              x-gena:
                auto-clear: 1
              x-hdl:
                type: autoclear
          - field:
              name: clearFaults
              range: 1
              preset: 0x0
              description: Clear fault statuses
              x-gena:
                auto-clear: 1
              x-hdl:
                type: autoclear
    - block:
        name: aurora
        description: Area for Aurora 64B/66B (duplex-mode) control and status registers
        address: next
        size: 12
        x-gena:
          reserved: false
        children:
          - reg:
              name: control
              description: Aurora control register
              width: 32
              access: rw
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
              children:
                - field:
                    name: reset
                    range: 0
                    preset: 0x0
                    description: Initiates Aurora core reset sequence
                    x-gena:
                      auto-clear: 1
                    x-hdl:
                      type: autoclear
                - field:
                    name: loopback
                    range: 3-1
                    preset: 0x0
                    description: Aurora loopback mode
                    comment: "0: Normal operation\n1: Near-end PCS loopback\n2: Near-end PMA loopback\n3: Reserved\n4: Far-end PMA loopback\n5: Reserved\n6: Far-end PCS loopback\n7: Reserved\n"
                - field:
                    name: gt_rxlpmen
                    range: 4
                    preset: 0x0
                    description: RX datapath
                    comment: "0: DFE (Decision Feedback Equalizer)\n1: LPM (Low Power Mode)\n"
                - field:
                    name: gt_rxrate
                    range: 7-5
                    preset: 0x0
                    description: Dynamic pins to automatically change effective PLL dividers in the GTH transceiver RX
                    comment: ""
                - field:
                    name: gt_txdiffctrl
                    range: 12-8
                    preset: 0x00
                    description: TX driver swing control
                    comment: "AFCZ\n0b00000: 144 mV\n0b00010: 216 mV\n0b00100: 286 mV\n0b00110: 356 mV\n0b01000: 427 mV\n0b01010: 498 mV\n0b01100: 568 mV\n0b01110: 638 mV\n0b10000: 708 mV\n0b10010: 777 mV\n0b10100: 844 mV\n0b10110: 909 mV\n0b11000: 971 mV\n0b11010: 1028 mV\n0b11100: 1079 mV\n0b11110: 1122 mV\n\nSIS\n0b0000: 170 mV\n0b0001: 250 mV\n0b0010: 320 mV\n0b0011: 390 mV\n0b0100: 460 mV\n0b0101: 530 mV\n0b0110: 600 mV\n0b0111: 660 mV\n0b1000: 730 mV\n0b1001: 780 mV\n0b1010: 840 mV\n0b1011: 900 mV\n0b1100: 950 mV\n0b1101: 1000 mV\n0b1110: 1040 mV\n0b1111: 1080 mV\n"
                - field:
                    name: gt_txpostcursor
                    range: 17-13
                    preset: 0x00
                    description: Transmitter post-cursor TX pre-emphasis control
                    comment: "0b00000: 0.00 dB\n0b00001: 0.22 dB\n0b00010: 0.45 dB\n0b00011: 0.68 dB\n0b00100: 0.92 dB\n0b00101: 1.16 dB\n0b00110: 1.41 dB\n0b00111: 1.67 dB\n0b01000: 1.94 dB\n0b01001: 2.21 dB\n0b01010: 2.50 dB\n0b01011: 2.79 dB\n0b01100: 3.10 dB\n0b01101: 3.41 dB\n0b01110: 3.74 dB\n0b01111: 4.08 dB\n0b10000: 4.44 dB\n0b10001: 4.81 dB\n0b10010: 5.19 dB\n0b10011: 5.60 dB\n0b10100: 6.02 dB\n0b10101: 6.47 dB\n0b10110: 6.94 dB\n0b10111: 7.43 dB\n0b11000: 7.96 dB\n0b11001: 8.52 dB\n0b11010: 9.12 dB\n0b11011: 9.76 dB\n0b11100: 10.46 dB\n0b11101: 11.21 dB\n0b11110: 12.04 dB\n0b11111: 12.96 dB\n"
                - field:
                    name: gt_txprecursor
                    range: 22-18
                    preset: 0x00
                    description: Transmitter pre-cursor TX pre-emphasis control
                    comment: "0b00000: 0.00 dB\n0b00001: 0.22 dB\n0b00010: 0.45 dB\n0b00011: 0.68 dB\n0b00100: 0.92 dB\n0b00101: 1.16 dB\n0b00110: 1.41 dB\n0b00111: 1.67 dB\n0b01000: 1.94 dB\n0b01001: 2.21 dB\n0b01010: 2.50 dB\n0b01011: 2.79 dB\n0b01100: 3.10 dB\n0b01101: 3.41 dB\n0b01110: 3.74 dB\n0b01111: 4.08 dB\n0b10000: 4.44 dB\n0b10001: 4.81 dB\n0b10010: 5.19 dB\n0b10011: 5.60 dB\n0b10100: 6.02 dB\n0b10101: 6.02 dB\n0b10110: 6.02 dB\n0b10111: 6.02 dB\n0b11000: 6.02 dB\n0b11001: 6.02 dB\n0b11010: 6.02 dB\n0b11011: 6.02 dB\n0b11100: 6.02 dB\n0b11101: 6.02 dB\n0b11110: 6.02 dB\n0b11111: 6.02 dB\n"
          - reg:
              name: status
              description: Aurora status register
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
              children:
                - field:
                    name: gtPllLock
                    description: Indicates that the transceiver has locked to a stable reference clock
                    range: 0
                - field:
                    name: laneUp
                    description: Asserted upon successful lane initialization
                    range: 1
                - field:
                    name: channelUp
                    description: Asserted when the channel is ready to send/receive data
                    range: 2
                - field:
                    name: initOk
                    description: Logical "and" of "gtPllLock", "laneUp" and "channelUp"
                    range: 3
                - field:
                    name: gtPllLockLatch
                    description: Stays latched low (0) when gtPllLock goes low (0)
                    range: 4
                - field:
                    name: laneUpLatch
                    description: Stays latched low (0) when laneUp goes low (0)
                    range: 5
                - field:
                    name: channelUpLatch
                    description: Stays latched low (0) when channelUp goes low (0)
                    range: 6
                - field:
                    name: hardErr
                    description: Hard error detected (asserted until the Aurora core resets)
                    range: 7
                - field:
                    name: softErr
                    description: Indicates that a soft error is detected in the incoming serial stream
                    range: 8
                - field:
                    name: hardErrLatch
                    description: Stays latched high (1) when hardErr goes high (1)
                    range: 9
                - field:
                    name: softErrLatch
                    description: Stays latched high (1) when softErr goes high (1)
                    range: 10
                - field:
                    name: gtRxBufStatus
                    description: RX buffer status
                    range: 13-11
                    comment: "0b000: Nominal condition\n0b001: Number of bytes in the buffer are less than CLK_COR_MIN_LAT (8)\n0b010: Number of bytes in the buffer are greater than CLK_COR_MAX_LAT (12)\n0b101: RX elastic buffer underflow\n0b110: RX elastic buffer overflow\n"
                - field:
                    name: gtTxBufStatus
                    description: TX buffer status
                    range: 15-14
                    comment: "txbufstatus[1]: TX buffer overflow or underflow status. When txbufstatus[1] is set High, the signal remains High until the TX buffer is reset.\n1: TX FIFO has overflow or underflow.\n0: No TX FIFO has overflow or underflow.\n\ntxbufstatus[0]: TX buffer fullness.\n1: TX FIFO is at least half full.\n0: TX FIFO is less than half full.\n"
                - field:
                    name: gtPowerGood
                    description: GT power good status
                    range: 16
          - reg:
              name: softErrCount
              description: Counts the number of "softErr" pulses
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
    - block:
        name: tx
        description: Area for TX control and status registers
        address: next
        size: 48
        x-gena:
          reserved: false
        children:
          - reg:
              name: control
              description: TX control register
              width: 32
              access: rw
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
              children:
                - field:
                    name: enable
                    description: Enables the TX
                    range: 0
                    preset: 0x0
                - field:
                    name: fixedClockMode
                    description: Enables the fixed-clock mode of the TX ('1') instead of default beam synchronous mode ('0')
                    range: 1
                    preset: 0x0
                - field:
                    name: interleavingEnable
                    description: Enables the interleaving of channel 2 with channel 1 (fixed-clock mode)
                    range: 2
                    preset: 0x0
                - field:
                    name: rateRatioCh1
                    description: Selection of data rate for interleaved channel 1 (fixed-clock mode)
                    range: 4-3
                    comment: "00: 125 MSps\n01: 62.5 MSps\n10: 31.25 MSps\n11: 15.625 MSps\n"
                - field:
                    name: rateRatioCh2
                    description: Selection of data rate for interleaved channel 2 (fixed-clock mode)
                    range: 6-5
                    comment: "00: 125 MSps\n01: 62.5 MSps\n10: 31.25 MSps\n11: 15.625 MSps\n"
                - field:
                    name: reset
                    range: 7
                    preset: 0x0
                    description: Global synchronous reset that puts all TX synchronous logic to a known state
                    x-gena:
                      auto-clear: 1
                    x-hdl:
                      type: autoclear
          - reg:
              name: numBuckets
              description: Number of SPS buckets, i.e. harmonic number h
              width: 32
              access: rw
              address: next
              preset: 0x120c
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: status
              description: Status register to read FIFO statuses and operation errors/faults
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
              children:
                - field:
                    name: wrErrLatched
                    description: Error writing to FIFOs (latched)
                    range: 1-0
                - field:
                    name: fullLatched
                    description: FIFOs full (latched)
                    range: 3-2
                - field:
                    name: empty
                    description: FIFOs empty
                    range: 5-4
                - field:
                    name: overflowLatched
                    description: CH1/CH2 FIFOs overflow (latched)
                    range: 7-6
                - field:
                    name: frevErrorLatched
                    description: FREV entry expected in FIFOs, but read different data (latched)
                    range: 9-8
                - field:
                    name: syncErrorLatched
                    description: SYNC entry expected in FIFOs, but read different data (latched)
                    range: 11-10
                - field:
                    name: rstDone
                    description: FIFOs reset sequence (both write and read channel) done correctly
                    range: 13-12
                - field:
                    name: numDataLowLatched
                    description: Too few data in the current turn (beam synchronous mode) (latched)
                    range: 15-14
                - field:
                    name: numDataHighLatched
                    description: Too much data in the current turn (beam synchronous mode) (latched)
                    range: 17-16
                - field:
                    name: configValid
                    description: A bit to indicate whether the TX configuration is valid or not (fixed-clock interleaving mode with rate ratios 0/0)
                    range: 18
          - reg:
              name: expectedNumDataTurnFifo0
              description: Expected number of FIFO 0 data per turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: expectedNumDataTurnFifo1
              description: Expected number of FIFO 1 data per turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: expectedNumPacketsTurn
              description: Expected number of M_AXIS_DATA_TX packets sent to Aurora per turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: fifoNumData0
              description: Instantaneous number of FIFO 0 data on in the current turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: fifoNumData1
              description: Instantaneous number of FIFO 1 data on in the current turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: fifoNumDataTurn0
              description: Latched number of FIFO 0 data on in previous turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: fifoNumDataTurn1
              description: Latched number of FIFO 1 data on in the previous turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
    - block:
        name: rx
        description: Area for RX control and status registers
        address: next
        size: 64
        x-gena:
          reserved: false
        children:
          - reg:
              name: control
              description: RX control register
              width: 32
              access: rw
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
              children:
                - field:
                    name: enable
                    description: Enables the RX
                    range: 0
                    preset: 0x0
                - field:
                    name: fixedClockMode
                    description: Enables the fixed-clock mode of the RX ('1') instead of default beam synchronous mode ('0')
                    range: 1
                    preset: 0x0
                - field:
                    name: interleavingEnable
                    description: Enables the interlaving of channel 2 with channel 1 (fixed-clock mode)
                    range: 2
                    preset: 0x0
                - field:
                    name: rateRatioCh1
                    description: Selection of data rate for interleaved channel 1 (fixed-clock mode)
                    range: 4-3
                    comment: "00: 125 MSps\n01: 62.5 MSps\n10: 31.25 MSps\n11: 15.625 MSps\n"
                - field:
                    name: rateRatioCh2
                    description: Selection of data rate for interleaved channel 2 (fixed-clock mode)
                    range: 6-5
                    comment: "00: 125 MSps\n01: 62.5 MSps\n10: 31.25 MSps\n11: 15.625 MSps\n"
                - field:
                    name: reset
                    range: 7
                    preset: 0x0
                    description: Global synchronous reset that puts all RX synchronous logic to a known state
                    x-gena:
                      auto-clear: 1
                    x-hdl:
                      type: autoclear
          - reg:
              name: numBuckets
              description: Number of SPS buckets, i.e. harmonic number h
              width: 32
              access: rw
              address: next
              preset: 0x120c
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: latency
              description: Selection of the fixed latency in ns (4ns is the smallest increment)
              width: 32
              access: rw
              address: next
              comment: "Range: 0x0002 to 0xFFFF (only 16 lowest bits of the register used).\nThe latency can't be below 2 (8ns).\n\nDefault/preset value: 200ns, corresponding to 50=0x32.\n"
              preset: 0x32
              x-fesa:
                multiplexed: true
                persistence: false
                min-val: 8
                max-val: 262143
              x-driver-edge:
                max-val: 0xFFFF
                min-val: 0x0002
              x-conversions:
                read: val*4
                write: floor(val/4)
          - reg:
              name: status
              description: Status register to read FIFO statuses and operation errors/faults
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
              children:
                - field:
                    name: bufferWrErrLatched
                    description: Error writing to buffer FIFO (latched)
                    range: 0
                - field:
                    name: wrErrLatched
                    description: Error writing to FIFOs (latched)
                    range: 2-1
                - field:
                    name: bufferFullLatched
                    description: Buffer FIFO full (latched)
                    range: 3
                - field:
                    name: fullLatched
                    description: FIFOs full (latched)
                    range: 5-4
                - field:
                    name: bufferEmpty
                    description: Buffer FIFO empty
                    range: 6
                - field:
                    name: empty
                    description: FIFOs empty
                    range: 8-7
                - field:
                    name: bufferOverflowLatched
                    description: Buffer FIFO overflow (latched)
                    range: 9
                - field:
                    name: overflowLatched
                    description: CH1/CH2 FIFOs overflow (latched)
                    range: 11-10
                - field:
                    name: bufferFrevErrorLatched
                    description: FREV entry expected in buffer FIFO, but read different data (latched)
                    range: 12
                - field:
                    name: frevErrorLatched
                    description: FREV entry expected in FIFOs, but read different data (latched)
                    range: 14-13
                    comment: "Tied to '0' in logic. Makes no sense to check for this error again, it is already checked in the buffer FIFO. Should be removed from the MM.\n"
                - field:
                    name: bufferSyncErrorLatched
                    description: SYNC entry expected in buffer FIFO, but read different data (latched)
                    range: 15
                - field:
                    name: syncErrorLatched
                    description: SYNC entry expected in FIFOs, but read different data (latched)
                    range: 17-16
                    comment: "Tied to '0' in logic. Makes no sense to check for this error again, it is already checked in the buffer FIFO. Should be removed from the MM.\n"
                - field:
                    name: bufferRstDone
                    description: Buffer FIFO reset sequence (both write and read channel) done correctly
                    range: 18
                - field:
                    name: rstDone
                    description: FIFOs reset sequence (both write and read channel) done correctly
                    range: 20-19
                - field:
                    name: bufferNumDataLowLatched
                    description: Too few data in the current turn (beam synchronous mode) (latched)
                    range: 21
                - field:
                    name: numDataLowLatched
                    description: Too few data in the current turn (beam synchronous mode) (latched)
                    range: 23-22
                    comment: "Tied to '0' in logic. Makes no sense to check for this error again, it is already checked in the buffer FIFO. Should be removed from the MM.\n"
                - field:
                    name: bufferNumDataHighLatched
                    description: Too much data in the current turn (beam synchronous mode) (latched)
                    range: 24
                - field:
                    name: numDataHighLatched
                    description: Too much data in the current turn (beam synchronous mode) (latched)
                    range: 26-25
                    comment: "Tied to '0' in logic. Makes no sense to check for this error again, it is already checked in the buffer FIFO. Should be removed from the MM.\n"
                - field:
                    name: configValid
                    description: A bit to indicate whether the RX configuration is valid or not (fixed-clock interleaving mode with rate ratios 0/0)
                    range: 27
                - field:
                    name: streamCutErrorLatched
                    description: Indicates that a DSP RX channel data stream has been cut at some point (fixed-clock mode)
                    range: 29-28
          - reg:
              name: expectedNumDataTurnFifo0
              description: Expected number of FIFO 0 data per turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: expectedNumDataTurnFifo1
              description: Expected number of FIFO 1 data per turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: expectedNumPacketsTurn
              description: Expected number of S_AXIS_DATA_RX packets received from Aurora per turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: fifoLevel0
              description: Instantaneous number of data in the FIFO 0
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: fifoLevel1
              description: Instantaneous number of data in the FIFO 1
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: bufferFifoNumData
              description: Instantaneous number of buffer FIFO data on in the current turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: fifoNumData0
              description: Instantaneous number of FIFO 0 data on in the current turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: fifoNumData1
              description: Instantaneous number of FIFO 1 data on in the current turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: bufferFifoNumDataTurn
              description: Latched number of buffer FIFO data on in previous turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: fifoNumDataTurn0
              description: Latched number of FIFO 0 data on in previous turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
          - reg:
              name: fifoNumDataTurn1
              description: Latched number of FIFO 1 data on in the previous turn (beam synchronous mode)
              width: 32
              access: ro
              address: next
              x-fesa:
                multiplexed: false
                persistence: false
