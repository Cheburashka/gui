memory-map:
  name: voltSetPoint
  bus: axi4-lite-32
  size: 4k
  word-endian: little
  schema-version:
    core: 2.0.0
    x-conversions: 1.0.0
    x-driver-edge: 1.0.0
    x-enums: 1.0.0
    x-fesa: 2.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-map-info: 1.0.0
    x-wbgen: 1.0.0
  x-map-info:
    memmap-version: 2.4.0
  x-hdl:
    bus-attribute: Xilinx
    name-suffix: _regs
    bus-granularity: byte
  children:
    - reg:
        name: control
        width: 32
        access: rw
        address: 0x0
        x-fesa:
          multiplexed: False
          persistence: False
        x-hdl:
          port: field
        children:
          - field:
              name: setpointEn
              description: enable setpoint (substractor at feedback input)
              range: 2
          - field:
              name: setpointDriveEn
              description: enable cavity fiiling (adder at feedbacks output)
              range: 3
          - field:
              name: staticSetpointEn
              description: enable static setpoint values (register) instead of functions data
              range: 4
          - field:
              name: economy_en
              description: enable full and dynamic economy
              range: 6
          - field:
              name: economy_reset
              description: reset the economy latch. Required at WRITEHW
              range: 7
              x-hdl:
                type: autoclear
          - field:
              name: rfOn
              description: Manually trigger RF On
              range: 8
              x-hdl:
                type: autoclear
          - field:
              name: rfOff
              description: Manually trigger RF Off
              range: 9
              x-hdl:
                type: autoclear
          - field:
              name: cavitySel
              description: Cavity setpoint selection in the RF train frame
              range: 12-10
          - field:
              name: am_enable
              description: enable amplitude modulation, AM signal given by RFNCO
              range: 13
          - field:
              name: longdamper_en
              description: enable longitudinal damper
              range: 14
          - field:
              name: longdamper_static_gain
              description: enable static gains for longitudinal damper
              range: 15
    - reg:
        name: staticSetpoint
        description: static setpoint values for chanel I and Q
        width: 32
        access: rw
        address: next
        type: signed
        x-fesa:
          multiplexed: True
          persistence: True
        children:
          - field:
              name: phase
              description: static setpoint value for phase
              range: 15-0
              type: signed
              x-conversions:
                read: val / pow(2.0, 15.0)*180.0
                write: val * pow(2.0, 15.0)/180.0
          - field:
              name: amplitude
              description: static setpoint value for amplitude
              range: 31-16
              type: signed
              x-conversions:
                read: val / pow(2.0, 15.0)
                write: val * pow(2.0, 15.0)
    - reg:
        name: status
        description: status of the voltage setPoint IP core
        width: 32
        access: ro
        address: next
        x-fesa:
          multiplexed: True
        children:
          - field:
              name: rfOn
              description: Show the RF on status. Toggled by rfOn, rfOff timing
              range: 0
          - field:
              name: longdamper_on
              description: Show the longgitudinal damper ON status. Toggled by ld On, ld Off timing
              range: 1
          - field:
              name: economy_on
              description: Show the economy (dynamic or full) ON status
              range: 2
    - block:
        name: gain
        address: next
        size: 32
        children:
          - reg:
              name: setpointDrive
              description: Cavity filling gain, range= +/-2
              width: 16
              type: signed
              access: rw
              address: next
              x-fesa:
                multiplexed: False
                persistence: True
                min-val: 0
                max-val: 2
              x-driver-edge:
                max-val: 32767
                min-val: -32768
              x-conversions:
                read: val / pow(2.0, 14.0)
                write: val * pow(2.0, 14.0)
          - reg:
              name: longdamper_dp
              description: longitudinal damper static gain dp,  range= +/-1
              width: 16
              type: signed
              access: rw
              address: next
              x-fesa:
                multiplexed: False
                persistence: True
                min-val: 0
                max-val: 1
              x-driver-edge:
                max-val: 32767
                min-val: 0
              x-conversions:
                read: val / pow(2.0, 15.0)
                write: val * pow(2.0, 15.0)
          - reg:
              name: longdamper_dv
              description: longitudinal damper static gain dV,  range= +/-1
              width: 16
              type: signed
              access: rw
              address: next
              x-fesa:
                multiplexed: False
                persistence: True
                min-val: 0
                max-val: 1
              x-driver-edge:
                max-val: 32767
                min-val: 0
              x-conversions:
                read: val / pow(2.0, 15.0)
                write: val * pow(2.0, 15.0)
          - reg:
              name: vcav_scaling
              description: Gain for normalizing Vcav sent to Beam control,  range= +/-1
              width: 16
              type: signed
              access: rw
              address: next
              preset: 0x7fff
              x-fesa:
                multiplexed: False
                persistence: True
                min-val: 0
                max-val: 1
              x-driver-edge:
                max-val: 32767
                min-val: 0
              x-conversions:
                read: val / pow(2.0, 15.0)
                write: val * pow(2.0, 15.0)
    - reg:
        name: ramp_rf_on
        width: 16
        access: rw
        description: Ramp time at RF on/off
        type: signed
        x-fesa:
          persistence: true
          multiplexed: false
          min-val: 8
          max-val: 262136
          unit: ns
        x-conversions:
          read: (pow(2.0, 15.0)-1) / val * 1/0.125
          write: (pow(2.0, 15.0)-1) / val * 1/0.125
        x-driver-edge:
          min-val: 1
          max-val: 32767
    - reg:
        name: ramp_am
        width: 16
        access: rw
        description: Ramp time for AM
        type: signed
        x-fesa:
          persistence: true
          multiplexed: false
          min-val: 8
          max-val: 262136
          unit: ns
        x-conversions:
          read: (pow(2.0, 15.0)-1) / val * 1/0.125
          write: (pow(2.0, 15.0)-1) / val * 1/0.125
        x-driver-edge:
          min-val: 1
          max-val: 32767
    - reg:
        name: amplitude_threshold
        width: 16
        access: rw
        description: Minimum voltage setpoint amplitude
        type: signed
        x-fesa:
          persistence: true
          multiplexed: false
          min-val: 0
          max-val: 1
        x-conversions:
          read: val / pow(2.0, 15.0)
          write: val * pow(2.0, 15.0)
        x-driver-edge:
          min-val: 0
          max-val: 32767