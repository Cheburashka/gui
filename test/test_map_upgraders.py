# Reksio - Memory Map Editor
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import sys
import unittest
import filecmp

from pathlib import Path

# Required as python_scripts are not an importable package
sys.path.append('python_scripts')
import upgrades

MEMMAP_PATH = {
    'RF_OPTCOMP':       'test/maps/RF_OPTCOMP',
    'RF_SPS200_LOOPS':  'test/maps/RF_SPS200_LOOPS'
}

class MapUpgradersTestCase(unittest.TestCase):
    def test_rf_optcomp(self):
        '''
        RF_OPTCOMP test for convertion from XML to cheby file.
        '''
        memmap_path = MEMMAP_PATH['RF_OPTCOMP']
        output_dir = Path(f'build/{memmap_path}')
        output_dir.mkdir(parents=True, exist_ok=True)

        # emulate calling CLI
        sys.argv = ['python_scripts/upgrades.py', '-d', str(memmap_path), '-o', str(output_dir)]
        upgrades.main() # should not fail

        expected_files = ['optoLinkPhiComp.cheby', 'smapLoopControl.cheby']
        for ff in expected_files:
            template_fpath = f'{memmap_path}/{ff}.expected'
            output_fpath = f'{output_dir}/{ff}'
            self.assertTrue(filecmp.cmp(template_fpath, output_fpath),
                            msg=f'Converted output file "{output_fpath}" and expected template "{template_fpath}" are different!')

    def test_rf_sps200_loops(self):
        '''
        RF_SPS200_LOOPS test for convertion from older cheby map to the latest schema.
        '''
        memmap_path = MEMMAP_PATH['RF_SPS200_LOOPS']
        output_dir = Path(f'build/{memmap_path}')
        output_dir.mkdir(parents=True, exist_ok=True)

        # emulate calling CLI
        sys.argv = ['python_scripts/upgrades.py', '-d', str(memmap_path), '-o', str(output_dir)]
        upgrades.main() # should not fail

        expected_files = [
            'sps200CavityControl.cheby',
            'wrc.cheby',
            'acqCore.cheby',
            'lm32.cheby',
            'GBLink.cheby',
            'app.cheby'
        ]
        for ff in expected_files:
            template_fpath = f'{memmap_path}/{ff}.expected'
            output_fpath = f'{output_dir}/{ff}'
            self.assertTrue(filecmp.cmp(template_fpath, output_fpath),
                            msg=f'Converted output file "{output_fpath}" and expected template "{template_fpath}" are different!')

if __name__ == '__main__':
    unittest.main()


