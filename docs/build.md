# Reksio compilation & building

Users can build Reksio locally or use the pre-compiled ready-to-run packages available in the section [Deploy -> Releases](https://gitlab.cern.ch/Cheburashka/reksio/-/releases).

## Requirements

Reksio is a cross-platform tool released on Windows 10 and Linux and requires the following to compile:
 - Qt 5.15,
 - C++17,
 - MSVC2022 (Windows),
 - CMake 3.20.X,
 - Python3.11.

## Linux

To build Reksio on Linux, first checkout the repo and run this script:

```
#!/bin/bash                                                                                            

mkdir -p gui-build                                                                                     
cd gui-build                                                                                           

cmake ..    -DCERN_BUILD=False \
            -DREKSIO_VERSION="${REKSIO_VERSION}" \
            -DCMAKE_BUILD_TYPE=RelWithDebInfo
            -DCOMMIT_ID="${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHA}" \
            -DCERN_BUILD=${CERN_BUILD} \
            -DREKSIO_VERSION=${CI_COMMIT_TAG} \
            -DPYCHEBY_VERSION=${PYCHEBY_VERSION} \
            -DCHEBY_VERSION=${CHEBY_VERSION} \
            -DCODE_GEN_VERSION=${CODE_GEN_VERSION}

make -j8                                                                                               
mv crash_handler/crash_handler ./bin_crash_handler
```

then to run Reksio, activate your local Python enviroment and launch:

```
./gui-build/reksio
```

## Windows

On Windows, the compilation process is a bit more complicated, as Reksio needs to download and unpack the embeddable Python package and budle it into the output package.

Assuming that the Qt libraries and tools are located in `C:\Qt\QT_VERSION` folder, to build Reksio, you can create a batch script:

```
mkdir "gui-build"
cd "gui-build"

cmake ../ -DCMAKE_BUILD_TYPE=RelWithDebInfo ^
            -G"Visual Studio 17 2022" ^
            -DCMAKE_PREFIX_PATH="C:\\Qt\\5.15.2" ^
            -DCOMMIT_ID="%CI_COMMIT_REF_NAME%_%CI_COMMIT_SHA%" ^
            -DCERN_BUILD=%CERN_BUILD% ^
            -DREKSIO_VERSION="%CI_COMMIT_TAG%" ^
            -DPYCHEBY_VERSION="%PYCHEBY_VERSION%" ^
            -DCHEBY_VERSION="%CHEBY_VERSION%" ^
            -DCODE_GEN_VERSION="%CODE_GEN_VERSION%" ^
            -DPYTHON_EXECUTABLE="C:\Python311\python.exe"

cmake --build . --config RelWithDebInfo
C:\\Qt\\5.15.2\\bin\\windeployqt.exe RelWithDebInfo\\reksio.exe

move schema RelWithDebInfo\\.
move python_scripts RelWithDebInfo\\.
move settings RelWithDebInfo\\.

curl -o ../python-3.11.X-embed-amd64.zip https://www.python.org/ftp/python/3.11.9/python-3.11.9-embed-amd64.zip
C:\\"Program Files"\\7-Zip\\7z.exe x ../python-3.11.X-embed-amd64.zip -oRelWithDebInfo\\.

move crash_handler\\RelWithDebInfo\\crash_handler.exe RelWithDebInfo\\bin_crash_handler.exe
```

> **WARNING**: CMake variable `DPYTHON_EXECUTABLE` must be defined on Windows, while Linux builds use the one provided on the `$PATH`.

