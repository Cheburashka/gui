#include "crashhandler.h"

#include <QApplication>

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    if(argc < 5)
        return -1;
    CrashHandler crashHandler;
    crashHandler.show();
    return app.exec();
}
