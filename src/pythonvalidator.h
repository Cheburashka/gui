/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef PYTHONVALIDATOR_H
#define PYTHONVALIDATOR_H
#include "validator.h"


class PythonValidator: public INodeRuleValidator
{
public:
    explicit PythonValidator(const YAML::Node& schema_node, const std::string& node_name);
    virtual bool validate(const std::string& value) const override;
    virtual bool validate(const Attribute* attr) const override;
    virtual bool validate(const MemoryNode* node) const;
    virtual std::string getMessage() const override;
private:
    std::string file_path;
};

class PyNodeEnumValidator : public INodeRuleValidator
{
public:
    explicit PyNodeEnumValidator(const YAML::Node& schema_node, const std::string& node_name);
    virtual bool validate(const std::string& value) const override;
    virtual bool validate(const Attribute* attr) const override;
    virtual std::string getMessage() const override;
    std::vector<std::string> getEnums(const Attribute* attr) const;
protected:
    std::string module_name;
};

#endif // PYTHONVALIDATOR_H
