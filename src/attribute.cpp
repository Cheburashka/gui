/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#include "attribute.h"
#include "memorynode.h"
#include "attributevalidator.h"
#include "attributecontainervalidator.h"
std::ostream& operator<< (std::ostream& stream, const Attribute& attribute)
{
    stream << attribute.getName() << ": " << attribute.getValue() << ";" << std::endl;
    return stream;
}

Attribute::Attribute(const std::string &name, const std::string& value):
    _name(name),
    _value(value),
    _savable(true)
{

}

bool Attribute::operator==(const Attribute &rhs) const
{
    return _name == rhs.getName() && _value == rhs.getValue() && isSavable() == rhs.isSavable();
}

bool Attribute::isValid() const
{
    return failed_validators.empty();
}

const std::string &Attribute::getValue() const
{
    return _value;
}

void Attribute::setValue(const std::string &value)
{
    _value = value;
    validate();
}

void Attribute::setParent(AttributeContainer* parent)
{
    _parent = parent;
}

AttributeContainer* Attribute::getParent() const
{
    return _parent;
}

MemoryNode *Attribute::getParentNode() const
{
    return _parent->getParentNode();
}

const std::string &Attribute::getName() const
{
    return _name;
}

std::vector<std::string> Attribute::getFullName() const
{
    std::vector<std::string> full_names = _parent->getFullName();
    full_names.push_back(getName());
    return full_names;
}

std::vector<INodeRuleValidator*> Attribute::validate() // return std::vector<std::pair<bool, std::shared_ptr<INodeRuleValidator>>>
{
    failed_validators.clear();
    if(_parent)
    {
        AttributeValidator* validator = getValidator();
        if(validator)
        {
            for(const auto& validator: validator->getValidators())
            {
                bool result = validator->validate(this);
                if(!result)
                    failed_validators.push_back(validator.get());
            }
        }
    }
    return failed_validators;
}

AttributeValidator* Attribute::getValidator() const
{
    if(_parent)
        return _parent->getValidator()->getAttributeValidator(_name);
    return nullptr;
}

const std::vector<INodeRuleValidator*> &Attribute::getFailedValidators() const
{
    return failed_validators;
}

bool Attribute::isSavable() const
{
    return _savable;
}

void Attribute::setSavable(const bool savable)
{
    _savable = savable;
}

bool Attribute::isEditable() const
{
    if(!_editable)
        return false;
    if(_parent)
    {
        const MemoryNode * parent_node = getParentNode();
        if(parent_node)
            return _editable && parent_node->isEditable();
    }
    return _editable;
}

void Attribute::setEditable(bool editable)
{
    _editable = editable;
}

bool Attribute::isDeprecated() const
{
    auto validator = getValidator();
    return validator->isDeprecated();
}
