/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef TREENODEOVERVIEWMODEL_H
#define TREENODEOVERVIEWMODEL_H

#include <QAbstractTableModel>
#include <memory>

class MemoryNode;
class Attribute;
class AttributeValidator;
class TreeNodeOverviewModel : public QAbstractTableModel
{
public:
    enum CustomItemDataRole
    {
      SortRole = Qt::UserRole
    };
    TreeNodeOverviewModel(const MemoryNode* parent_node, const std::string& child_type, QObject* parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;


private:
    const MemoryNode* root_node;
    const std::string& child_type;
    QVariant getAttributeValue(const Attribute* attr, int role) const;
    std::vector<AttributeValidator*> visible_attributes;
    friend class WidgetTab;
};

#endif // TREENODEOVERVIEWMODEL_H
