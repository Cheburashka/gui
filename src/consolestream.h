/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef CONSOLESTREAM_H
#define CONSOLESTREAM_H
#include <QTextEdit>
#include <iostream>
#include <string>
#include <streambuf>
#include <QLineEdit>

class ConsoleStream : public std::basic_streambuf<char>
{
public:
    ConsoleStream(std::ostream& stream/*, QTextEdit* text_edit*/);
    virtual ~ConsoleStream() override;
    static void registerMyConsoleMessageHandler();
private:
    static void myConsoleMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg);
    virtual int_type overflow(int_type v) override;
    virtual std::streamsize xsputn(const char* p, std::streamsize n) override;

    std::ostream& _stream;
    std::streambuf* _old_buf;
    QTextEdit * _console;
};

#endif // CONSOLESTREAM_H
