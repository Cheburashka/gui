/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef CUSTOMNODESVIEW_H
#define CUSTOMNODESVIEW_H
#include <QTreeView>
#include <QMenu>
#include <QUndoStack>
#include <memory>
#include "commands.h"

class NodesModel; // fwd decl
class AttributesModel; // fwd decl
class MemoryNode; // fwd decl
class ValidatorNode; // fwd decl

class CustomNodesView : public QTreeView
{
    Q_OBJECT
public:
    explicit CustomNodesView(QWidget* parent = nullptr);
    void dropEvent(QDropEvent *event) override;
    void dragMoveEvent(QDragMoveEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;
    void copy(const QModelIndexList& indexes);
    void cut(const QModelIndexList& indexes);
    void paste(const QModelIndex& index);
    NodesModel* getNodesModel();
    void setUndoStack(QUndoStack* stack);
    QUndoStack* getUndoStack();
    AttributesModel* getAttributesModel(const QModelIndex& nodesIndex);
    void populateModel(std::unique_ptr<MemoryNode> root, ValidatorNode* validator);

    void clear();


public Q_SLOTS:
    // context menu
    void onCustomContextMenu(const QPoint & point);

    void addChild(const QString& child_type);
    void addChild(const QString& child_type, const int position);
    void addChild(const QString& child_type, const QModelIndex& parent_index);
    void addChild(const QString& child_type, const QModelIndex& parent_index, const int position);

    void addAttribute(const QString& attribute_name);
    void addAttribute(const QString& attribute_name, const QModelIndex& parent_index);
    void addRequiredAttributes(const QModelIndex& parent_index);

    void duplicateNodes();
    void removeNodes();
    void moveUpNode();
    void moveDownNode();

private:
    void copyOrCut(const QModelIndexList& indexes, const Qt::DropAction& action);
    void moveNodes(MoveCommand::MoveDirection dir);
private:
    QMenu* treeContextMenu;
    QMenu* add_submenu;
    QMenu* add_child_submenu;
    QMenu* add_attribute_submenu;
    QMenu* remove_attribute_submenu;
    QMenu* insert_after_sibling_submenu;
    QMenu* insert_before_sibling_submenu;
    QMenu* python_actions_submenu;
    QUndoStack *undoStack;
Q_SIGNALS:
    void modelChanged();
};

#endif // CUSTOMNODESVIEW_H
