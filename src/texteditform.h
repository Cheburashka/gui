/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef TEXTEDITFORM_H
#define TEXTEDITFORM_H

#include <QDialog>

namespace Ui {
class TextEditForm;
}

class TextEditForm : public QDialog
{
    Q_OBJECT

public:
    explicit TextEditForm(const QString& text, QWidget *parent = nullptr);
    ~TextEditForm() override;
    QString getText() const;
    QSize sizeHint() const override;
    bool wasChanged() const;
public Q_SLOTS:
    void setChanged();

private:
    Ui::TextEditForm *ui;
    bool changed;
};

#endif // TEXTEDITFORM_H
