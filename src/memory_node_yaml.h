/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef MEMORY_NODE_YAML_H
#define MEMORY_NODE_YAML_H
#include <yaml-cpp/yaml.h>

class AttributeContainer;
class MemoryNode;
class AttributeContainerValidator;
class ValidatorNode;

namespace YAML {

Node encode_impl(const AttributeContainer& rhs);
bool decode_impl(const Node& node, AttributeContainer& rhs, MemoryNode* root_node = nullptr);
Node encode_impl(const MemoryNode& rhs);
bool decode_impl(const Node& node, MemoryNode& rhs);
bool decode_impl(const Node& node, AttributeContainerValidator& rhs);
bool decode_impl(const Node& node, ValidatorNode& rhs);

template<>
struct convert<AttributeContainer> {
    static Node encode(const AttributeContainer& rhs)
    {
        return encode_impl(rhs);
    }

    static bool decode(const Node& node, AttributeContainer& rhs)
    {
        return decode_impl(node, rhs);
    }
};


template<>
struct convert<MemoryNode> {
    static Node encode(const MemoryNode& rhs)
    {
        return encode_impl(rhs);
    }

    static bool decode(const Node& node, MemoryNode& rhs)
    {
        return decode_impl(node, rhs);
    }
};
template<>
struct convert<AttributeContainerValidator> {
    static bool decode(const Node& node, AttributeContainerValidator& rhs)
    {
        return decode_impl(node, rhs);
    }
};
// validatorNode
template<>
struct convert<ValidatorNode> {
//    static Node encode(const ValidatorNode& rhs)
//    {
//        Node root;

//        // its a mapping by default?
//        root["type"] = "map";
//        Node mapping = root["mapping"];
//        if(!rhs.children.empty())
//        {
//            for(const auto& child: rhs.children)
//            {
//                mapping[child.first] = encode_children(*child.second.get());
//            }
//        }
//        return root;
//    }
//    static Node encode_children(const ValidatorNode& rhs)
//    {
//        Node root;
//        root["type"] = "map";
//        Node mapping = root["mapping"];
//        for(const auto& attrib : rhs.attributes)
//        {
//            Node attribute_node = mapping[attrib.first];
//            for(const auto& validator : attrib.second)
//            {
//                // type validator
//                const NodeTypeValidator* type_validator = dynamic_cast<const NodeTypeValidator*>(validator.get());
//                if(type_validator)
//                {
//                    attribute_node["type"] = type_validator->getTargetType();
//                }
//            }
//        }
//        if(!rhs.children.empty())
//        {
//            Node children = mapping["children"];
//            children["type"] = "seq";
//            Node sequence = children["sequence"];
//            for(const auto& child: rhs.children)
//            {
//                sequence.push_back(encode_child(*child.second.get(), child.first));
//            }
//        }
//        return root;
//    }
//    static Node encode_child(const ValidatorNode&rhs, const std::string& name)
//    {
//        Node root;
//        root["type"] = "map";
//        Node mapping = root["mapping"];
//        mapping[name] = encode(rhs);
//        return root;
//    }
    static bool decode(const Node& node, ValidatorNode& rhs)
    {
        return decode_impl(node, rhs);
    }
};

} // namespace YAML
#endif // MEMORY_NODE_YAML_H
