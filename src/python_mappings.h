/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef PYTHON_MAPPINGS_H
#define PYTHON_MAPPINGS_H

#include <pybind11/stl.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl_bind.h>
#include <pybind11/embed.h>
#include <iostream>

#include <QDebug>

namespace py = pybind11;

class PyStdErrOutStreamRedirect {
    py::object _stdout;
    py::object _stderr;
    py::object _stdout_buffer;
    py::object _stderr_buffer;
public:
    explicit PyStdErrOutStreamRedirect() {
        auto sysm = py::module::import("sys");
        _stdout = sysm.attr("stdout");
        _stderr = sysm.attr("stderr");
        auto my_stringio = py::module::import("reksio").attr("StringIOLikeRedir");
        _stdout_buffer = my_stringio();  // Other filelike object can be used here as well, such as objects created by pybind11
        _stderr_buffer = my_stringio();
        sysm.attr("stdout") = _stdout_buffer;
        sysm.attr("stderr") = _stderr_buffer;
    }

    ~PyStdErrOutStreamRedirect() {
        try
        {
            auto sysm = py::module::import("sys");
            sysm.attr("stdout") = _stdout;
            sysm.attr("stderr") = _stderr;
        }
        catch (const std::exception& e)
        {
            qWarning() << QString("Exception in ~PyStdErrOutStreamRedirect(): ") + e.what();
        }
        catch (...)
        {
            qWarning() << QString("Exception in ~PyStdErrOutStreamRedirect()");
        }
    }
};

struct PyStringIOLikeRedirecting {
    // noop does nothing!
    void noop(py::args, py::kwargs) {}
    size_t write(const std::string& str) { std::cout << str; return str.size();}
};

#endif // PYTHON_MAPPINGS_H
