/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#include "utils.h"



QModelIndexList getOnlyColumn0(QModelIndexList indexes)
{
    QModelIndexList only_col_0_indexes;
    std::copy_if(std::make_move_iterator(indexes.begin()),
                 std::make_move_iterator(indexes.end()),
                 std::back_inserter(only_col_0_indexes),
                 [](const QModelIndex& index){return index.column() == 0;});
    return only_col_0_indexes;
}

bool checkIfSameParent(const QModelIndexList &indexes)
{
    if(indexes.size() < 2)
        return true;
    const QModelIndex& parent = indexes.front().parent();
    const auto& result = std::find_if(indexes.begin()+1, indexes.end(), [&parent](const auto& n){return n.parent() != parent;});
    return result == indexes.end();
}
