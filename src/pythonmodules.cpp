/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#include "pythonmodules.h"

bool PythonModules::import(const std::string &module_name)
{
    try
    {
        pybind11::module module = pybind11::module::import(module_name.c_str());
        modules.insert(std::make_pair(module_name, module));
    }
    catch(const std::exception& exc)
    {
        qWarning("Failed to import %s: %s", module_name.c_str(), exc.what());
        return false;
    }
    return true;
}

pybind11::module PythonModules::getModule(const std::string &module_name)
{
    return modules.at(module_name);
}

void PythonModules::reload(const std::string &module_name)
{
    getModule(module_name).reload();
}

void PythonModules::reload()
{
    for(auto& kv: modules)
    {
        kv.second.reload();
    }
}
