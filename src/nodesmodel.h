/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QUndoStack>
#include <QUndoCommand>
#include <QHash>
#include <memory>

class MemoryNode; // fwd decl
class AttributesModel;
class NodesModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum CustomRoles
    {
        InternalPointer = Qt::UserRole,
        AttributeModelData
    };
    NodesModel(std::unique_ptr<MemoryNode> root, QUndoStack * undoStack, QObject* parent = nullptr);
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    bool addChild(int position, const QString type, const QModelIndex& parent);
    bool duplicateChild(int targetPosition, const QModelIndex& source_index);
    bool duplicateChild(int targetPosition, const QModelIndex& source_index, const QModelIndex& target_index);
    bool insertChild(int position, std::unique_ptr<MemoryNode> child, const QModelIndex& parent);
    bool insertChild(int position, MemoryNode* child, const QModelIndex& parent);

    std::unique_ptr<MemoryNode> removeChild(int row, const QModelIndex& parent = QModelIndex());

    bool moveRows(const QModelIndex& sourceParent, int sourcePosition, int count, const QModelIndex& destinationParent, int destPosition) override;
    bool moveRows(const QModelIndex& sourceParent, int sourcePosition, const QModelIndex& destinationParent, int destPosition);
    bool moveRows(const QModelIndex& sourceParent, const QModelIndex& sourceIndex, const QModelIndex& destinationParent, const QModelIndex& destinationIndex);

    Qt::ItemFlags flags(const QModelIndex &index) const override;
    Qt::DropActions supportedDropActions() const override;
    Qt::DropActions supportedDragActions() const override;
    QStringList mimeTypes() const override;
    QMimeData* mimeData(const QModelIndexList& indexes, Qt::DropAction action) const;
    QMimeData* mimeData(const QModelIndexList &indexes) const override;
    bool dropMimeData(const QMimeData* mime_data, Qt::DropAction action, int row, int column, const QModelIndex & parent) override;
    MemoryNode* getItem(const QModelIndex& index) const;

    QUndoStack* undoStack() const;

    AttributesModel* getAttributesModel(const QModelIndex& parent);
    void clearNotUsedAttributesModels();

    bool isValid(const QModelIndex& index) const;

    bool isEditable(const QModelIndex& index) const;

    static NodesModel* getNodesModel(const QModelIndex& index);

    QModelIndex getIndex(MemoryNode* node) const;
    QModelIndexList getIndexes(MemoryNode* node) const;
    QModelIndexList getIndexes(MemoryNode* node, const QModelIndex& start) const;
    void getAllChildren(const QModelIndex& index, QModelIndexList& index_list) const;

    MemoryNode* getRoot();

    bool isModified(const QModelIndex& index, bool recursive = true) const;
    std::vector<QModelIndex> getModifiedIndexes() const;
    bool hasChild(const QModelIndex& parent, const QModelIndex& child) const;
    bool isParent(const QModelIndex& parent, const QModelIndex& child) const;

    void save(const QModelIndex& root, const QString& filename) const;
private:
    bool checkCommand(const QModelIndex& index, const QUndoCommand* command, bool recursive) const;
    void populateModifiedIndexes(const QUndoCommand* command, std::vector<QModelIndex>& indexes) const;
public Q_SLOTS:
    // generic
    void validate(const QModelIndex& index, const bool recursive=true);
    // overload for dataChanged
    void validate(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles);
    // overload for rowsInserted, rowsRemoved
    void validate(const QModelIndex& parent, int first, int last);

private:
    std::unique_ptr<MemoryNode> rootItem = nullptr;
    QUndoStack* _undoStack;
    QString mime_type = "text/yaml"; //"application/x-yamltreenode"
    QHash<MemoryNode*, QVariant> attribute_models;
private:
    // don't use these
    using QAbstractItemModel::insertRow;
    using QAbstractItemModel::insertRows;
    using QAbstractItemModel::removeRow;
    using QAbstractItemModel::removeRows;
};

#endif // TREEMODEL_H
