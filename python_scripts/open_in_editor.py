# Reksio - Memory Map Editor
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import reksio
from pathlib import Path


def open_main_map(main_window):
    if main_window.hasFile():
        reksio.openExplorer(str(main_window.currentFile))


def action(node):
    submap_path_attr = node.getAttribute(['computed', 'file_path'])
    if submap_path_attr is None:
        reksio.warn("Cannot open file editor - the path is unknown!")
        return
    path = Path(submap_path_attr.value)
    if not path.exists():
        reksio.warn("Cannot open file editor - the path does not exist!")
        return
    reksio.openExplorer(str(path))
