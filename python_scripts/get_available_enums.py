# Reksio - Memory Map Editor
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import reksio


def get_memory_map_parent(node):
    parent = node.parent
    while parent is not None:
        if parent.type == 'memory-map':
            return parent
        parent = parent.parent


def get_enums(attribute):
    attr_node = attribute.parentNode
    memory_map_parent = get_memory_map_parent(attr_node)
    x_enums = memory_map_parent.getChildByType('x-enums')
    if x_enums:
        return [enum.name for enum in x_enums.children()]
    return []
