# Reksio - Memory Map Editor
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import reksio


def validate(attr):
    attribute_name = ["x-driver-edge", "generate-separate-library"]
    module_type_name = ["x-driver-edge", "module-type"]
    attr_node = attr.parentNode
    generate_separate = attr.value.lower() == "true"
    is_submap = attr_node.type == "submap"

    if generate_separate:
        if is_submap:
            memory_map_node = attr_node.getChildByType("memory-map")
            if memory_map_node:
                if not memory_map_node.getAttribute(module_type_name):
                    reksio.warn("x-driver-edge/generate-separate-library set to true, "
                                         "but there's no x-driver-edge/module-type!")
                    return False
            else:
                # not loaded ?
                return True
        else:
            # is memory map

            # check parent if it's not overwritten
            parent = attr_node.parent
            if parent and parent.type == "submap":
                parent_generate_separate = parent.getAttribute(attribute_name)
                if parent_generate_separate and parent_generate_separate.value.lower() == "false":
                    # overwritten by parent
                    return True
            if not attr_node.getAttribute(module_type_name):
                reksio.warn("x-driver-edge/generate-separate-library set to true, "
                                     "but there's no x-driver-edge/module-type!")
                return False
    return True


def getMessage():
    return "x-driver-edge/generate-separate-library is set, but there's no module-type!"

