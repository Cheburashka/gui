import reksio

def get_enums(attribute):
    attr_node = attribute.parentNode
    root_map = attr_node.getRoot().children().pop()
    address_spaces = filter(lambda c: c.type == 'address-space', root_map.children())

    if address_spaces:
        l = [space.getAttribute(['name']).value for space in address_spaces]
        return l

    return []
